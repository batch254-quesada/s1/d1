package com.zuitt.wdc044.controllers;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class PostController {

    @Autowired
    PostService postService;

    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    // ResponseEntity represents the whole HTTP response: status code, headers, and body
    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post) {

        // We can access the "postService" methods and pass the following arguments:
        // stringToken of the current session will be retrieved from the request header
        // a "post" object will be instantiated upon receiving the request body, and this will follow the properties defined in the Post model

        // note: the "key" name of the request from Postman should be similar to the property names defined in the model
        postService.createPost(stringToken, post);

        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }

    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Post>> getPosts() {
//        Iterable<Post> posts = postService.getPosts();
//
//        for (Post post : posts) {
//            post.getUser();
//        }
//        return ResponseEntity.ok().body(posts);

        return new ResponseEntity<Iterable<Post>>(postService.getPosts(), HttpStatus.OK);
    }

    // Update a post
    @RequestMapping(value = "/posts/{postId}", method = RequestMethod.PUT)
    // @PathVariable is used for data passed in the URI
        // @RequestParam vs @PathVariable
        // @RequestParam is used to extract data found in the query parameters (commonly used for filtering the results based on the condition)
        // @PathVariable is used to extract exact record
    public ResponseEntity<Object> updatePost(@PathVariable Long postId, @RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post) {

        return postService.updatePost(postId, stringToken, post);
    }

    // Delete a post
    @RequestMapping(value = "/posts/{postId}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long postId, @RequestHeader(value = "Authorization") String stringToken) {
        return postService.deletePost(postId, stringToken);
    }

    // Retrieve authenticated user's posts
    @RequestMapping(value = "/myPosts", method = RequestMethod.GET)
    public ResponseEntity<Object> getUserPosts(@RequestHeader(value = "Authorization") String stringToken) {
        return new ResponseEntity<>(postService.getUserPosts(stringToken), HttpStatus.OK);
    }
}
