package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@SpringBootApplication

// allows access to REST methods that we use to manage endpoints
@RestController

public class Wdc044Application {

	public static void main(String[] args) {
		SpringApplication.run(Wdc044Application.class, args);
	}

	// @RequestParam is used to extract query parameters, form parameter, and even files from the request
	// http://localhost:8080/


	// @GetMapping - similar to router in express.js
	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {

		// format method is used to output returned results in the browser
		return String.format("Hello %s!", name);
	}

	@GetMapping("/greetings")
	public String greetings(@RequestParam(value = "greet", defaultValue = "Baby ko") String greet) {

		return String.format("Kumusta ka na, %s?", greet);
	}

	// ./mvnw spring-boot:run ---- similar to npm start in React app


	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	@GetMapping("/hi")
	public String hi(@RequestParam(value = "name", defaultValue = "user") String name) {

		return String.format("Kumusta ka na, %s?", name);
	}

	@GetMapping("/nameage")
	public String nameAge(@RequestParam(value = "name", defaultValue = "Chris") String name, @RequestParam(value = "age", defaultValue = "26") int age) {

		return String.format("What's up %s? You are %d right?", name, age);
	}

}
